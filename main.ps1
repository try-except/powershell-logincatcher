<#
World's simplest login-catcher
Author: Try/Except

This is the dumbest little script I could come up with to register login times.
It is not intended for serious use, since it only logs the time a user account
was logged in.

In the future I may add some more fancy stuff, like failed login attempts and
whatnot.
#>

# Recieve the out_path as a paramater
param(
    [Parameter(Mandatory=$true)]  # Make out_path a mandatory parameter
    [string]$OutPath
)
$timestamp = Get-Date -DisplayHint DateTime
$out_string = Out-String -InputObject $timestamp
#echo $out_path

Add-Content -Path $OutPath -Value ($out_string.Trim())
